extern crate env_logger;
extern crate log;

extern crate rand;

extern crate rusoto_core;
extern crate rusoto_sns;

extern crate serde;
extern crate serde_json;

mod services;

use std::error::Error;

use log::info;

use rand::Rng;

use services::sns_service;
use services::sns_service::OutboundMessage;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();

    let topic_arn = std::env::var("TOPIC_ARN").unwrap();

    let msg = generate_random_message();
    info!("msg: {:?}", msg);

    sns_service::publish_message(topic_arn, msg).await?;

    info!("all done");
    Ok(())
}

fn generate_random_message() -> OutboundMessage {
    let mut rng = rand::thread_rng();
    let qty: u8 = rng.gen();

    let material = String::from("gold");

    OutboundMessage {
        qty: qty.to_string(),
        material,
        message_type: Some(String::from("TYPE_ONE")),
    }
}
