use std::collections::HashMap;

use log::{debug, error, info};

use rusoto_core::{Region, RusotoError};
use rusoto_sns::{MessageAttributeValue, PublishError, PublishInput, Sns, SnsClient};

use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct OutboundMessage {
    pub qty: String,
    pub material: String,
    pub message_type: Option<String>,
}

pub async fn publish_message(
    topic_arn: String,
    msg: OutboundMessage,
) -> Result<(), RusotoError<PublishError>> {
    info!("call to publish message");

    let msg_string = serde_json::to_string(&msg)?;

    let mut message_attributes: HashMap<String, MessageAttributeValue> = HashMap::new();

    match msg.message_type {
        Some(val) => {
            message_attributes.insert(
                String::from("TYPE"),
                MessageAttributeValue {
                    binary_value: None,
                    data_type: String::from("String"),
                    string_value: Some(val),
                },
            );
        }
        None => {}
    };

    let input = PublishInput {
        message: msg_string,
        message_attributes: Some(message_attributes),
        message_structure: None,
        phone_number: None,
        subject: None,
        target_arn: None,
        topic_arn: Some(topic_arn),
    };

    debug!("input: {:?}", input);

    let client = SnsClient::new(Region::UsEast1);
    match client.publish(input).await {
        Ok(val) => {
            info!("message published");
            debug!("sns publish response: {:?}", val);
            return Ok(());
        }
        Err(e) => {
            error!("error publishing message: {:?}", e);
            return Err(e);
        }
    }
}
